import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BillhistoryPage } from './billhistory';

@NgModule({
  declarations: [
    BillhistoryPage,
  ],
  imports: [
    IonicPageModule.forChild(BillhistoryPage),
  ],
})
export class BillhistoryPageModule {}
