import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import {DailyreportPage} from '../dailyreport/dailyreport';
import {MonthlyreportPage} from '../monthlyreport/monthlyreport';
import {AnnualreportPage} from '../annualreport/annualreport';
import { DatePicker } from '@ionic-native/date-picker';

  // Project Name:Emobill
  // Semantic Version:0.0.1
  // Author Name:Santosh 
  // Client Name:xyz

@IonicPage()
@Component({
  selector: 'page-salesreport',
  templateUrl: 'salesreport.html',
})
export class SalesreportPage {
  testRadioOpen:any
  testRadioResult:any

  constructor(public navCtrl: NavController, public navParams: NavParams,public alertCtrl: AlertController) {
  }

  from =false;
to=false;      //Declaration
customer=false;

  ionViewDidLoad() {
    console.log('ionViewDidLoad SalesreportPage');
  }


  fromdate(){
    this.from=true; //from date
   }
 
   
   todate(){         //to date 
 this.to=true;
   }
 
 
   custonumber(){     //customer contact number 
 this.customer=true;
   }
 
 
 
   //submit button 
   submitinput(){
    
   if (this.from==false) {
     alert("eneter from date"); //alert for not selecting date
   }
 
   else if(this.to==false)
   {
   alert("enter to date");      // alert for not selecting to date
   }
   
 else if(this.customer==false)
 {
   alert("enter branch name"); // alert for not entering branch 
 }
  
 }
 //submit button end
 
 


  //  NAVIGATING TO THE  DailyreportPage
  dailyreport(){
    this.navCtrl.push(DailyreportPage);
  }


  //  NAVIGATING TO THE  MonthlyreportPage
  monthlyreport(){
    this.navCtrl.push(MonthlyreportPage);

  }


//  NAVIGATING TO THE  AnnualreportPage
  annualreport(){
    this.navCtrl.push(AnnualreportPage);
  }




    // SEND ENQUIRE RADIO ALERT.......
  sendenquire() {
    let alert = this.alertCtrl.create();
    alert.setTitle('SEND QUERY');

    alert.addInput({
      type: 'radio',
      label: 'CUSTOMERS DATA',   //CUSTOMER DATE SELECTION
      value: 'blue',
      checked:true
    
    })
    
    alert.addInput({
      type: 'radio',
      label: 'EMPLOYEES  DATA', //EMPLOYEES  DATA SELECTION
      value: 'blue',
      checked: false
    
    })

    alert.addInput({
      type: 'radio',
      label: 'SALES REPORTS',   //SALES REPORTS SELECTION
      value: 'black',
      checked: false
    
    })
    
    alert.addInput({
      type: 'radio',
      label: 'BILL HISTORY',  //BILL HISTORY SELECTION
      value: 'black',
      checked: false
    
    })


    alert.addInput({
      type: 'radio',
      label: 'EXPENSES', //EXPENSES  SELECTION
      value: 'black',
      checked: false
    
    })
    ;

    
    alert.addButton({
      text: 'YES',               // YES 
      handler: data => {
        this.showAlert();
      }

    });
    alert.addButton('NO') //NO
    
  
    ;
    alert.present();
  }           // SEND ENQUIRE RADIO ALERT END........



  showAlert() {
    let alert = this.alertCtrl.create({
    subTitle: 'Request sent successfully',// ALERT AFTER SELECTING PARTUCULAR  OPTION
    buttons: ['OK']
  });
  alert.present();
}

}
