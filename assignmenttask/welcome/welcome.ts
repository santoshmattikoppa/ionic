import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';



  // Project Name:Emobill
  // Semantic Version:0.0.1
  // Author Name:Santosh 
  // Client Name:xyz
  
@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
})
export class WelcomePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WelcomePage');
  }

  start(){
this.navCtrl.setRoot(LoginPage);

  }

}
