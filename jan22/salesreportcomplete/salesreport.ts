import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import {DailyreportPage} from '../dailyreport/dailyreport';
import {MonthlyreportPage} from '../monthlyreport/monthlyreport';
import {AnnualreportPage} from '../annualreport/annualreport';
import { DatePicker } from '@ionic-native/date-picker';

/**
 * Generated class for the SalesreportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-salesreport',
  templateUrl: 'salesreport.html',
})
export class SalesreportPage {
  testRadioOpen:any
  testRadioResult:any

  constructor(public navCtrl: NavController, public navParams: NavParams,public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SalesreportPage');
  }


  //  NAVIGATING TO THE  DailyreportPage
  dailyreport(){
    this.navCtrl.push(DailyreportPage);
  }


  //  NAVIGATING TO THE  MonthlyreportPage
  monthlyreport(){
    this.navCtrl.push(MonthlyreportPage);

  }


//  NAVIGATING TO THE  AnnualreportPage
  annualreport(){
    this.navCtrl.push(AnnualreportPage);
  }




    // SEND ENQUIRE RADIO ALERT HERE..........
  sendenquire() {
    let alert = this.alertCtrl.create();
    alert.setTitle('SEND QUERY');

    alert.addInput({
      type: 'radio',
      label: 'CUSTOMERS DATA',   //CUSTOMER DATE SELECTION
      value: 'black',
      checked: false
    
    })
    
    alert.addInput({
      type: 'radio',
      label: 'EMPLOYEES  DATA', //EMPLOYEES  DATA SELECTION
      value: 'black',
      checked: false
    
    })

    alert.addInput({
      type: 'radio',
      label: 'SALES REPORTS',   //SALES REPORTS SELECTION
      value: 'black',
      checked: false
    
    })
    
    alert.addInput({
      type: 'radio',
      label: 'BILL HISTORY',  //BILL HISTORY SELECTION
      value: 'black',
      checked: false
    
    })


    alert.addInput({
      type: 'radio',
      label: 'EXPENSES', //EXPENSES  SELECTION
      value: 'black',
      checked: false
    
    })
    ;

    
    alert.addButton({
      text: 'YES',               // YES 
      handler: data => {
        this.testRadioOpen = false;
        this.testRadioResult = data;
      }

    });
    alert.addButton('NO') //NO
    
  
    ;
    alert.present();
  }           // SEND ENQUIRE RADIO ALERT END HERE..........






}
