import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

/**
 * Generated class for the TestprinterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-testprinter',
  templateUrl: 'testprinter.html',
})
export class TestprinterPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TestprinterPage');
  }

 

  // TEST PRINTER ALERT MESSAGE HERE............
  testprinter() {
    let prompt = this.alertCtrl.create({
      // title: '',
      message: "Some app requires Bluetooth turned on.",
      inputs: [
        // {
        //   name: 'title',
        //   placeholder: 'Title'
        // },
      ],
      buttons: [
        {
          text: 'Decline',
          handler: data => {
            console.log('Decline clicked');
          }
        },
        {
          text: 'Allow',
          handler: data => {
            console.log('Allow clicked');
          }
        }
      ]
    });
    prompt.present();
  }

  // TEST PRINTER ALERT MESSAGE END HERE............





}
