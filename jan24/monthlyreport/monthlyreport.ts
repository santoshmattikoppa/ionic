import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';



  // Project Name:Emobill
  // Semantic Version:0.0.1
  // Author Name:Santosh 
  // Client Name:xyz


@IonicPage()
@Component({
  selector: 'page-monthlyreport',
  templateUrl: 'monthlyreport.html',
})
export class MonthlyreportPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MonthlyreportPage');
  }

}
