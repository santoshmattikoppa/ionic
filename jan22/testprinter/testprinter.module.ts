import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TestprinterPage } from './testprinter';

@NgModule({
  declarations: [
    TestprinterPage,
  ],
  imports: [
    IonicPageModule.forChild(TestprinterPage),
  ],
})
export class TestprinterPageModule {}
