import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  toggleStatus=false;
  constructor(public navCtrl: NavController, public navParams: NavParams,public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  // back(){
  //   this.navCtrl.pop();
  // }






  //  ENABLE TAX FUNCTION ALERT HERE.......

  enabletax() {
    this.toggleStatus=true;
    if(this.toggleStatus==true){
      let prompt = this.alertCtrl.create({
        title: 'ENTER TAX',
       
        inputs: [
          {
            name: 'title',
            placeholder: 'ENTER TAX',
        
          },
        ],
        buttons: [
          
          {
            text: 'SAVE',
            handler: data => {
              console.log('save clicked');
            //  this.onCancel();
  
            }
          },
          {
            text: 'CANCEL',
            handler: data => {
              console.log('cancel clicked');
              // this.onCancel();
            }
          }
        ]
      });
      prompt.present();
     
    }
    

   
  }
  
 //  ENABLE TAX FUNCTION ALERT END HERE.......



//  onCancel(){

//   this.toggleStatus=false;
//  }



  
  //  GST NUMBER FUNCTION ALERT HERE.......

  gstnumber(){
    let prompt = this.alertCtrl.create({
      title: 'ENTER GST NO',
     
      inputs: [
        {
          name: 'title',
          placeholder: 'ENTER GST NUMBER',
      
        },
      ],
      buttons: [
        
        {
          text: 'SAVE',
          handler: data => {
            console.log('save clicked');

          }
        },
        {
          text: 'CANCEL',
          handler: data => {
            console.log('cancel clicked');
            
          }
        }
      ]
    });
    prompt.present();
  }

 //  GST NUMBER FUNCTION ALERT END HERE.......









   //  APP LOCK FUNCTION ALERT HERE.......

   applock(){
    let prompt = this.alertCtrl.create({
      title: 'ENTER APP LOCK PASSWORD',
     
      inputs: [
        {
          name: 'title',
          placeholder: 'ENTER PASSWORD',
      
        },
      ],
      buttons: [
        
        {
          text: 'SAVE',
          handler: data => {
            console.log('save clicked');

          }
        },
        {
          text: 'CANCEL',
          handler: data => {
            console.log('cancel clicked');
            
          }
        }
      ]
    });
    prompt.present();
  }
   //  APP LOCK FUNCTION ALERT END HERE.......









   //  REFERAL DISCOUNT  FUNCTION ALERT HERE.......

   referaldiscount(){
    let prompt = this.alertCtrl.create({
      title: 'ENTER REFERRAL DISCOUNT',
     
      inputs: [
        {
          name: 'title',
          placeholder: 'ENTER DISCOUNT',
      
        },
      ],
      buttons: [
        
        {
          text: 'SAVE',
          handler: data => {
            console.log('save clicked');

          }
        },
        {
          text: 'CANCEL',
          handler: data => {
            console.log('cancel clicked');
            
          }
        }
      ]
    });
    prompt.present();
  }

//  REFERAL DISCOUNT  FUNCTION ALERT END HERE.......







   //  BILL PREFIX  FUNCTION ALERT HERE.......

   billprefix(){
    let prompt = this.alertCtrl.create({
      title: 'BILL PREFIX',
     
      inputs: [
        {
          name: 'title',
          placeholder: 'ENTER BILL PREFIX',
      
        },
      ],
      buttons: [
        
        {
          text: 'SAVE',
          handler: data => {
            console.log('save clicked');

          }
        },
        {
          text: 'CANCEL',
          handler: data => {
            console.log('cancel clicked');
            
          }
        }
      ]
    });
    prompt.present();
  }
 //  BILL PREFIX  FUNCTION ALERT END HERE.......






   //  ADD TIN NUMBER  FUNCTION ALERT HERE.......

   addtinnumber(){
    let prompt = this.alertCtrl.create({
      title: 'ADD TIN NUMBER',
     
      inputs: [
        {
          name: 'title',
          placeholder: 'ENTER TIN NUMBER',
      
        },
      ],
      buttons: [
        
        {
          text: 'SAVE',
          handler: data => {
            console.log('save clicked');

          }
        },
        {
          text: 'CANCEL',
          handler: data => {
            console.log('cancel clicked');
            
          }
        }
      ]
    });
    prompt.present();
  }
//  ADD TIN NUMBER  FUNCTION ALERT END HERE.......








 //  ENTER INVOICE PREFIX  FUNCTION ALERT HERE.......

 invoiceprefix(){
  let prompt = this.alertCtrl.create({
    title: 'ENTER INVOICE PREFIX',
   
    inputs: [
      {
        name: 'title',
        placeholder: 'ENTER INVOICE PREFIX',
    
      },
    ],
    buttons: [
      
      {
        text: 'SAVE',
        handler: data => {
          console.log('save clicked');

        }
      },
      {
        text: 'CANCEL',
        handler: data => {
          console.log('cancel clicked');
          
        }
      }
    ]
  });
  prompt.present();
}
 //  ENTER INVOICE PREFIX  FUNCTION ALERT END HERE.......




}
