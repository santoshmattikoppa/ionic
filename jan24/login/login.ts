import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AlertController } from 'ionic-angular';
 
  // Project Name:Emobill
  // Semantic Version:0.0.1
  // Author Name:Santosh 
  // Client Name:xyz

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
 
  username:any
   isInputDisabled:boolean=false;
    disabled =true;

  constructor(public navCtrl: NavController, public navParams: NavParams,public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  back(){
    this.navCtrl.pop();
  }



  editpage(){

    this.disabled =false;
  }

  savechenge(){

    let prompt = this.alertCtrl.create({
      
      message: "User Profile Modified Successfully",
    
      buttons: [
       
        {
          text: 'OK',
          handler: data => {
            console.log('OK clicked');
          }
        }

      ]
    });
    prompt.present();
  }




}


