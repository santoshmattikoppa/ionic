import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

/**
 * Generated class for the LoginnewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-loginnew',
  templateUrl: 'loginnew.html',
})
export class LoginnewPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginnewPage');
  }

  // FORGOT PASSWORD ALERT MESSAGE HERE

  forgotalert() {
    let prompt = this.alertCtrl.create({
      title: 'FORGOT PASSWORD',
      message: "",
      inputs: [
        {
          name: '',
          placeholder: 'ENTER MOBILE NUMBER' //ENTER MOBILE HERE......
        },
      ],
      buttons: [
        {
          text: 'SEND',
          handler: data => {
            console.log('Send clicked');
            this.showAlert();     //SEND ALERT MESSAGE HERE......
          }
        },
        {
          text: 'CANCEL',
          handler: data => {
            console.log('Cancel clicked');
            
          }
        }
      ]
    });
    prompt.present();
  }

  showAlert() {
    let alert = this.alertCtrl.create({
      subTitle: 'Activation Code has been sent to your registered Mobile Number',// ALERT AFTER SENDING ACTIVATION CODE TO THE REGISTERED MOBILE NUMBER...
      buttons: ['OK']
    });
    alert.present();
  }
    
 // FORGOT PASSWORD ALERT MESSAGE ENDS HERE




}
