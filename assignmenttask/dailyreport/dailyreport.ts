import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-dailyreport',
  templateUrl: 'dailyreport.html',
})
export class DailyreportPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DailyreportPage');
  }

}


  // Project Name:Emobill
  // Semantic Version:0.0.1
  // Author Name:Santosh 
  // Client Name:xyz