import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

  // Project Name:Emobill
  // Semantic Version:0.0.1
  // Author Name:Santosh 
  // Client Name:xyz

@IonicPage()
@Component({
  selector: 'page-billhistory',
  templateUrl: 'billhistory.html',
})
export class BillhistoryPage {

from =false;
to=false;     //Declaration 
customer=false;
 
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BillhistoryPage');
  }


  fromdate(){
   this.from=true; //from date
  }

  
  todate(){         //to date 
this.to=true;
  }


  custonumber(){     //customer contact number 
this.customer=true;
  }



  //submit button 
  submitinput(){
   
  if (this.from==false) {
    alert("eneter from date"); //alert for not selecting date
  }

  else if(this.to==false)
  {
  alert("enter to date");      // alert for not selecting to date
  }
  
else if(this.customer==false)
{
  alert("enter customer contact number"); // alert for not entering contact number 
}
 
}
//submit button end




}
