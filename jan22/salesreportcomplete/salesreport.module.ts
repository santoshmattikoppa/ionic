import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SalesreportPage } from './salesreport';

@NgModule({
  declarations: [
    SalesreportPage,
  ],
  imports: [
    IonicPageModule.forChild(SalesreportPage),
  ],
})
export class SalesreportPageModule {}
