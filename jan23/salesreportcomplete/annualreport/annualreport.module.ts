import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AnnualreportPage } from './annualreport';

@NgModule({
  declarations: [
    AnnualreportPage,
  ],
  imports: [
    IonicPageModule.forChild(AnnualreportPage),
  ],
})
export class AnnualreportPageModule {}
