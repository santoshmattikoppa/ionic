import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {LoginPage} from '../login/login';
import {RegisterPage} from '../register/register';
import {SalesreportPage} from '../salesreport/salesreport';
import {BillhistoryPage} from '../billhistory/billhistory';
import {MysubscriptionPage} from '../mysubscription/mysubscription';
import {TestprinterPage} from '../testprinter/testprinter';
import {LoginnewPage} from '../loginnew/loginnew';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  start(){

    this.navCtrl.push(LoginPage)
  }
  freetrail(){
    this.navCtrl.push(RegisterPage)
  } 

  salesreport(){
    this.navCtrl.push(SalesreportPage)
  }

  billhistory(){
    this.navCtrl.push(BillhistoryPage);
  }
  mysubscription(){
    this.navCtrl.push(MysubscriptionPage);
  }
  testprinter(){
    this.navCtrl.push(TestprinterPage);
  }

  loginnew(){
    this.navCtrl.push(LoginnewPage);
  }
}
